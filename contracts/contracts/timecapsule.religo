type title = string;
type description = string;
type storage = big_map (title, description);

type returnType = (list(operation), storage);
let owneraddr : address =
  ("tz1iQf3Ar3gc5A35LCpS9vggTAPvZeeK1e3M" : address);

type action = 
| CreateItem ((title, description))
| RemoveItem ((title, description))

let createItem = ((title, description, s) : (title, description, storage)) : returnType => 
([]: list(operation), Big_map.add(title, description, s))

let removeItem = ((title, s) : (title, storage)) : returnType => 
      if (Tezos.sender != owneraddr) { //if the contract submitter is not me
    (failwith("ACCESS_DENIED_NOT_OWNER") : (list (operation), storage))
  } else { //if it is me, remove it from the map
 ([]: list(operation), Big_map.remove(title, s))
  };

let main = ((action,s): (action, storage)) : returnType => {
    switch (action) {
      | CreateItem (n) => createItem(n[0], n[1], s)
| RemoveItem (n) => removeItem(n[0], s)
    };
};
