import React, { useEffect } from "react";
import { TezosToolkit } from "@taquito/taquito";
import styles from './Header.module.css';
import './globals.css';
import { useWallet } from "./hooks/use-wallet";
import { useBalanceState } from "./hooks/use-balance-state";
import { useContract } from "./hooks/use-contract";


export default function App() {
  const tezos = new TezosToolkit("https://testnet-tezos.giganode.io");

  const {
    initialized,
    address,
    error: walletError,
    loading: walletLoading,
    connect: connectToWallet,
  } = useWallet(tezos);
  const {
    storage,
    error: contractError,
    loading: contractLoading,
    contract,
    operationsCount,
    connect: connectToContract,
    increaseOperationsCount,
  } = useContract(tezos);
  
  //const {
  //  balance,
  //  error: balanceError,
  //  loading: balanceLoading,
  //} = useBalanceState(tezos, address, operationsCount);

  const [operationLoading, setOperationLoading] = React.useState(false);
  const [operationError, setOperationError] = React.useState("");

  //use storage.name, storage.value, address, balance
  useEffect(() => {
    if (storage) {
      //setName(storage.name);
      //setAge(storage.age);
    }
  }, [storage]);

  return (
    <div class="header text-center mx-auto lg:w-2/4 py-24">

      <nav class="border-bottom border border-gray-100 bg-white py-3 px-5 fixed top-0 left-0 right-0 flex lg:flex-row flex-col justify-between items-center">
        <img src="/blocksoftimelogo.jpeg" width="281" height="54" alt="BlocksOfTime logo" />
        <ul class="flex lg:flex-row flex-col lg:px-0 px-3 items-center h-full justify-center">
          <li class="lg:relative lg:top-auto lg:left-auto absolute top-6 left-3">
          </li>
          <button class="border border-blue-500 text-blue-500 hover:bg-blue-500 hover:text-white rounded-md px-3 py-2 "
            onClick={connect}> Connect </button>
          <li className="lg:ml-3">
            <pre className="bg-gray-700 text-white whitespace-pre-line lg:whitespace-nowrap lg:rounded-full rounded-md px-3 py-2 lg:text-sm text-xs">
              🔏 Your Digital Pen: {address}
            </pre>
          </li>
        </ul>
      </nav>

    </div>
  );

  async function connect() {
    await connectToWallet();
    await connectToContract();
  }

  async function submit(name, age) {
    if (!contract) {
      return;
    }
    try {
      const setAllOp = await contract.methods.setAll(name, Number(age)).send();
      await setAllOp.confirmation();
    } catch (error) {
      setOperationError(error.message);
    }
  }
}
